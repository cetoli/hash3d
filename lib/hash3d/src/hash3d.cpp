/*
 Copyright (c) 2014-present PlatformIO <contact@platformio.org>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
**/

#include <hash3d.h>
#include <Arduino.h>
//#include <Vector.h>

int* Hash3d::shuffle()
{
    int array[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 };

    Vector<int> intVect;

    Vector<int> intTemp;

    // Fill the vector with the contents of the array
    intVect.Assign(array, 27);
    intTemp.Assign(array, 27);

    // Add another few elements to the array

    intVect.Clear();

    while (intTemp.Size() > 0) {
        int idx = random(0, intTemp.Size());
        int card = intTemp[idx];
        intVect.PushBack(card);
        intTemp.Erase(idx);
    }
    return intVect.Data();
}


void Hash3d::println(char *strBuf)
{
    Serial.println(strBuf);
}

Hash3d::Hash3d()
{
    int array[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 };

    // Fill the vector with the contents of the array
    intOut.Assign(array, 27);

}

void Hash3d::fullBoard()
{

  // Fill the vector with the contents of the array
  int *pos = shuffle();
  intPos.Assign(pos, 27);
  intBit.Assign(shuffle(), 27);

    while (intPos.Size() > 0) {
        int idx = intPos.Back();
        int card = intBit.Back();
        intOut[idx] = card;
    }

}

void Hash3d::printVector(Vector <int> v, int i)
{

    // Fill the vector with the contents of the array
    char strBuf[90];
    sprintf(strBuf, "%d %d %d %d %d %d %d %d %d", v[i+0], v[i+1], v[i+2], v[i+3], v[i+4], v[i+5], v[i+6], v[i+7], v[i+8]);
    println(strBuf);

}

void Hash3d::printAll()
{
    char strNovaLeitura[] = "Nova Leitura";
    fullBoard();
    println(strNovaLeitura);
    printVector(intOut, 0);
    printVector(intOut, 9);
    printVector(intOut, 18);
    //delay(500);               // wait for a second
}
int Hash3d::add(int a, int b)
{
    return a + b;
}

int Hash3d::sub(int a, int b)
{
    return a - b;
}

int Hash3d::mul(int a, int b)
{
    return a * b;
}


int Hash3d::div(int a, int b)
{
    return a / b;
}
