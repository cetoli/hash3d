/*
 Copyright (c) 2014-present PlatformIO <contact@platformio.org>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
**/

#include <hash3d.h>
#include <unity.h>
#include <Arduino.h>

char strThetext[] = { "1 2 3 4 5 6 7 8 9 10 11 12 13 14,15 16 17 18 19 20 21 22 23 24 25 26 27" };
void lcopy(char* src, char* dst, int len) {
    memcpy(dst, src, sizeof(src[0])*len);
}


class MockHash3d : public Hash3d {
  public:
    MockHash3d(){
    int array[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 };

    // Fill the vector with the contents of the array
    intOut.Assign(array, 27);

};
    void printSect(int sect){
        printVector(intOut, sect);
    };   
    virtual void println(char *strBuf){
        lcopy(strThetext, strBuf, 9);
    };   
  private:
};
MockHash3d calc;

// void setUp(void) {
// // set stuff up here
// }

// void tearDown(void) {
// // clean stuff up here
// }

void lprintVector(int *v)
{

    // Fill the vector with the contents of the array
    char strBuf[90];
    int i = 0;
    sprintf(strBuf, "%d %d %d %d %d %d %d %d %d", v[i+0], v[i+1], v[i+2], v[i+3], v[i+4], v[i+5], v[i+6], v[i+7], v[i+8]);
    lcopy(strThetext, strBuf, 9);

}
void test_shuffle_cube_array(void) {
    //Hash3d calc();
    int *cube = calc.shuffle();
    TEST_ASSERT_EQUAL(26, cube[2]);
}

void test_shuffle_cube_array_all(void) {
    //Hash3d calc();
    int *cube = calc.shuffle();
    int move[] = {0,7,12,2,20,1,18,15,24,4,21,8,13,27,9,5,6,17,11,22,23,3,16,14,26,10,25};
    // printVector(cube);
    for (int i=0; i<27; i++){
        TEST_ASSERT_EQUAL(move[i], cube[i]);

    }
}

void test_shuffle_cube_array_string(void) {
    //Hash3d calc();
    int *cube = calc.shuffle();
    char *move = "0 18 27 23 19 6 25 8 4";
    lprintVector(cube);
    TEST_ASSERT_EQUAL_STRING(move, strThetext); //"0 1 2 3 4");

}

void test_cube_array_string_printAll(void) {
    lcopy(strThetext, "0 1 2 3 4 6 5 8 7", 9);
    //Hash3d calc();
    calc.printAll();
    char *move = "0 18 27 23 19 6 25 8 4";
    TEST_ASSERT_EQUAL_STRING(move, strThetext); //"0 1 2 3 4");
}

void test_cube_array_string_printHigh(void) {
    lcopy(strThetext, "0 1 2 3 4 6 5 8 7", 9);
    //Hash3d calc();
    calc.printSect(0);
    char *move = "0 18 27 23 19 6 25 8 4";
    TEST_ASSERT_EQUAL_STRING(move, strThetext); //"0 1 2 3 4");
}

void test_cube_array_string_printMid(void) {
    lcopy(strThetext, "0 1 2 3 4 6 5 8 7", 9);
    //Hash3d calc();
    calc.printSect(9);
    char *move = "0 18 27 23 19 6 25 8 4";
    TEST_ASSERT_EQUAL_STRING(move, strThetext); //"0 1 2 3 4");
}

void test_function_calculator_addition(void) {
    TEST_ASSERT_EQUAL(32, calc.add(25, 7));
}

void test_function_calculator_subtraction(void) {
    TEST_ASSERT_EQUAL(20, calc.sub(23, 3));
}

void test_function_calculator_multiplication(void) {
    TEST_ASSERT_EQUAL(50, calc.mul(25, 2));
}

void test_function_calculator_division(void) {
    TEST_ASSERT_EQUAL(32, calc.div(100, 3));
}

int main(int argc, char **argv) {
    UNITY_BEGIN();
    RUN_TEST(test_function_calculator_addition);
    RUN_TEST(test_function_calculator_subtraction);
    RUN_TEST(test_function_calculator_multiplication);
    RUN_TEST(test_shuffle_cube_array);
    RUN_TEST(test_shuffle_cube_array_all);
    // RUN_TEST(test_shuffle_cube_array_string);
    // RUN_TEST(test_cube_array_string_printAll);
    RUN_TEST(test_cube_array_string_printHigh);
    RUN_TEST(test_cube_array_string_printMid);
    UNITY_END();

    return 0;
}
